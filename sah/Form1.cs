﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace sah
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<string, patrat> patrate = new Dictionary<string, patrat>();
        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    PictureBox p = new PictureBox();
                    p.BackgroundImageLayout = ImageLayout.Stretch;
                    p.Location = new System.Drawing.Point(75 * j + 25, 75 * i + 25);
                    string ID = "";
                    switch (j)
                    {
                        case 0: ID = "A";
                            break;
                        case 1: ID = "B";
                            break;
                        case 2: ID = "C";
                            break;
                        case 3: ID = "D";
                            break;
                        case 4: ID = "E";
                            break;
                        case 5: ID = "F";
                            break;
                        case 6: ID = "G";
                            break;
                        case 7: ID = "H";
                            break;
                    }

                    ID = ID + (i + 1);
                    p.Name = ID;
                    bool negru;
                    p.Size = new System.Drawing.Size(75, 75);
                    if ((i % 2 == 0 && j % 2 == 0) || (i % 2 == 1 && j % 2 == 1))
                    {
                        p.BackColor = Color.White;
                        negru = false;
                    }
                    else
                    {
                        p.BackColor = Color.Gray;
                        negru = true;
                    }
                    p.TabIndex = 0;
                    p.TabStop = false;
                    p.Visible = true;
                    p.BorderStyle = BorderStyle.None;
                    Controls.Add(p);
                    patrate.Add(ID, new patrat(ID, p, 0, 0, negru, j + 1));
                    p.MouseDown += new System.Windows.Forms.MouseEventHandler(this.p_MouseDown);
                    p.MouseUp += new System.Windows.Forms.MouseEventHandler(this.p_MouseUp);
                }

            set("A2", 1, 2);
            set("B2", 1, 2);
            set("C2", 1, 2);
            set("D2", 1, 2);
            set("E2", 1, 2);
            set("F2", 1, 2);
            set("G2", 1, 2);
            set("H2", 1, 2);
            //set("A1", 2, 2);
            //set("B1", 3, 2);
            //set("C1", 4, 2);
            //set("D1", 5, 2);
            //set("E1", 6, 2);
            //set("F1", 4, 2);
            //set("G1", 3, 2);
            //set("H1", 2, 2);

            set("A7", 1, 1);
            set("B7", 1, 1);
            set("C7", 1, 1);
            set("D7", 1, 1);
            set("E7", 1, 1);
            set("F7", 1, 1);
            set("G7", 1, 1);
            set("H7", 1, 1);
            //set("A8", 2, 1);
            //set("B8", 3, 1);
            //set("C8", 4, 1);
            //set("D8", 5, 1);
            //set("E8", 6, 1);
            //set("F8", 4, 1);
            //set("G8", 3, 1);
            //set("H8", 2, 1);
            // 1=pion , 2 = turn, 3 = cal , 4 = nebun, 5 = regina, 6 = rege
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\pion2.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\turn2.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\cal2.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\nebun2.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\regina2.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\rege2.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\pion2_n.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\turn2_n.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\cal2_n.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\nebun2_n.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\regina2_n.png"));
            imageList1.Images.Add(Image.FromFile(Application.StartupPath + "\\images\\rege2_n.png"));
        }
        private void p_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                PictureBox p = (PictureBox)sender;
                if (patrate.ContainsKey(p.Name))
                {
                    if (patrate[p.Name].patrat_negru)
                        p.BackColor = Color.DarkGreen;
                    else
                        p.BackColor = Color.GreenYellow;
                }
            }
        }
        Random rnd = new Random();
        List<mutari_computer> mutari = new List<mutari_computer>();
        public void computer_move()
        {
            mutari.Clear();

            foreach (patrat p in patrate.Values)
            {
                int nr = Convert.ToInt16(p.nume.Substring(1, 1));

                if (p.culoare != culoarea_mea && p.culoare != 0 && p.tip == 1)
                {
                    if (nr == 2)
                    {
                        if (patrate[p.nume[0] + "3"].culoare == 0)
                        mutari.Add(new mutari_computer(p.nume, p.nume[0] + "3", get_scor(p.nume[0] + "3")));
                        if (patrate[p.nume[0] + "4"].culoare == 0)
                        mutari.Add(new mutari_computer(p.nume, p.nume[0] + "4", get_scor(p.nume[0] + "4")));
                    }
                    else if (nr > 2 && nr != 8)
                    {
                        if (patrate[p.nume[0].ToString() + (nr + 1)].culoare == 0)
                        {
                            mutari.Add(new mutari_computer(p.nume, p.nume[0].ToString() + (nr + 1), get_scor(p.nume[0].ToString() + (nr + 1))));
                        }

                        string s = getcoloana(p.coloana - 1) + (nr + 1);
                        if (!getcoloana(p.coloana - 1).Equals("") && patrate[s].culoare == culoarea_mea)
                        {
                            mutari.Add(new mutari_computer(p.nume, s, get_scor(s)));
                        }

                        s = getcoloana(p.coloana + 1) + (nr + 1);
                        if (!getcoloana(p.coloana + 1).Equals("") && patrate[s].culoare == culoarea_mea)
                        {
                            mutari.Add(new mutari_computer(p.nume, s, get_scor(s)));
                        }
                    }
                }
            }
            int max = -1000;
            List<mutari_computer> mutari_optime = new List<mutari_computer>();
            foreach (mutari_computer m in mutari)
                if (m.scor > max)
                    max = m.scor;

            foreach (mutari_computer m in mutari)
                if (m.scor == max)
                    mutari_optime.Add(m);

            mutari_computer c;
            if (mutari_optime.Count > 0)
            {
                try
                {
                    c = mutari_optime[rnd.Next(0, mutari_optime.Count - 1)];
                }
                catch
                {
                    c = mutari_optime[0];
                }

                set(c.to, 1, 2);
                history(2, 1, c.from, c.to);
                set(c.from, 0, 0);

                check_finish();
            }
            else
                MessageBox.Show("Remiza - nu mai sunt mutari posibile .");
            

        }

        public int get_scor(string p)
        {
            int scor = 0;
            int nr = Convert.ToInt16(p.Substring(1, 1));
            string s = getcoloana(patrate[p].coloana - 1) + (nr -1);
            if (patrate.ContainsKey(s) && patrate[s].culoare == 2)
                scor = scor + 1;
            s = getcoloana(patrate[p].coloana + 1) + (nr - 1);
            if (patrate.ContainsKey(s) && patrate[s].culoare == 2)
                scor = scor + 1;

            s = getcoloana(patrate[p].coloana - 1) + (nr + 1);
            if (patrate.ContainsKey(s) && patrate[s].culoare == 1)
                scor = scor - 2;
            s = getcoloana(patrate[p].coloana + 1) + (nr + 1);
            if (patrate.ContainsKey(s) && patrate[s].culoare == 1)
                scor = scor - 2;

            s = getcoloana(patrate[p].coloana - 1) + (nr + 1);
            if (patrate.ContainsKey(s) && patrate[s].culoare == 2)
                scor = scor + 1;
            s = getcoloana(patrate[p].coloana + 1) + (nr + 1);
            if (patrate.ContainsKey(s) && patrate[s].culoare == 2)
                scor = scor + 1;

            if (patrate[p].culoare == 1)
                scor = scor + 3;

            return scor;
        }

        private void check_finish()
        {
            bool ok = false;
            int pioni_player = 0;
            int pioni_computer = 0;
            foreach (patrat p in patrate.Values)
            {
                int nr = Convert.ToInt16(p.nume.Substring(1, 1));
                if (p.culoare == 1)
                {
                    if (nr != 1)
                        ok = true;
                    pioni_player++;
                }
            }

            if (!ok)
            {
                foreach (patrat p in patrate.Values)
                {
                    if (p.culoare == 2)
                        pioni_computer++;
                }

                if (pioni_computer > pioni_player)
                    MessageBox.Show("Computerul castiga.");
                else if (pioni_computer < pioni_player)
                    MessageBox.Show("Ai castigat." + pioni_computer + " " + pioni_player );
                else MessageBox.Show("Remiza");
            }

        }

        List<string> mutari_posibile = new List<string>();
        private void p_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = (PictureBox)sender;
            foreach (string pp in mutari_posibile)
            {
                clear_mutare_posibila(pp);
            }
            if (patrate.ContainsKey(p.Name))
            {
                if (mutari_posibile.Contains(p.Name) && last_picturebox.culoare == culoarea_mea)
                {
                    set(p.Name, last_picturebox.tip, last_picturebox.culoare);
                    history(last_picturebox.culoare, last_picturebox.tip, p.Name, last_picturebox.nume);
                    set(last_picturebox.nume, 0, 0);

                    computer_move();
                    if (last_picturebox != null)
                    {

                        if (patrate[last_picturebox.nume].patrat_negru)
                            patrate[last_picturebox.nume].p.BackColor = Color.Gray;
                        else
                            patrate[last_picturebox.nume].p.BackColor = Color.White;

                    }
                }
                else
                {
                    if (patrate[p.Name].culoare == culoarea_mea)
                    {
                        if (last_picturebox != null)
                        {
                            if (patrate[last_picturebox.nume].patrat_negru)
                                patrate[last_picturebox.nume].p.BackColor = Color.Gray;
                            else
                                patrate[last_picturebox.nume].p.BackColor = Color.White;

                            if (patrate[p.Name].culoare == culoarea_mea)
                            {
                                if (patrate[p.Name].patrat_negru)
                                    p.BackColor = Color.DarkGreen;
                                else
                                    p.BackColor = Color.GreenYellow;
                            }
                        }
                        last_picturebox = patrate[p.Name];
                        mutari_posibile.Clear();
                        if (patrate[p.Name].tip == 1)
                        {
                            mutari_pion(p.Name);
                        }
                        else if (patrate[p.Name].tip == 2)
                        {
                            mutari_turn(p.Name);
                        }
                        else if (patrate[p.Name].tip == 4)
                        {
                            mutari_nebun(p.Name);
                        }
                        else if (patrate[p.Name].tip == 5)
                        {
                            mutari_nebun(p.Name);
                            mutari_turn(p.Name);
                        }
                        else if (patrate[p.Name].tip == 3)
                        {
                            mutari_cal(p.Name);
                        }
                        else if (patrate[p.Name].tip == 6)
                        {
                            mutari_rege(p.Name);
                        }
                    }
                    else
                        if (patrate[p.Name].patrat_negru)
                            p.BackColor = Color.Gray;
                        else
                            p.BackColor = Color.White;
                }
            }
        }

        public void mutari_rege(string Name)
        {
            int nr = Convert.ToInt16(Name.Substring(1, 1));

            int i = nr;
            int j = patrate[Name].coloana + 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr+1;
            j = patrate[Name].coloana;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr - 1;
            j = patrate[Name].coloana;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr;
            j = patrate[Name].coloana - 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr - 1;
            j = patrate[Name].coloana - 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr + 1;
            j = patrate[Name].coloana + 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr - 1;
            j = patrate[Name].coloana + 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr + 1;
            j = patrate[Name].coloana - 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

        }

        public void mutari_cal(string Name)
        {
            int nr = Convert.ToInt16(Name.Substring(1, 1));

            int i = nr - 2;
            int j = patrate[Name].coloana + 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr - 2;
            j = patrate[Name].coloana - 1;

            if (i >= 1 && j >= 1 && i<=8 && j<=8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr - 1;
            j = patrate[Name].coloana - 2;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr - 1;
            j = patrate[Name].coloana + 2;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);


            i = nr + 2;
            j = patrate[Name].coloana - 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr + 2;
            j = patrate[Name].coloana + 1;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr + 1;
            j = patrate[Name].coloana + 2;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);

            i = nr + 1;
            j = patrate[Name].coloana - 2;

            if (i >= 1 && j >= 1 && i <= 8 && j <= 8)
                if ((patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0) || patrate[getcoloana(j) + i].culoare == 0)
                    mutare_posibila(getcoloana(j) + i);
        }

        public void mutari_nebun(string Name)
        {
            int nr = Convert.ToInt16(Name.Substring(1, 1));

            int i = nr+1;
            int j = patrate[Name].coloana+1;

            while (i <= 8 && j <= 8)
            {

                if (patrate[getcoloana(j) + i].culoare == culoarea_mea)
                    break;
                else if (patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0)
                {
                    mutare_posibila(getcoloana(j) + i);
                    break;
                }
                else
                    mutare_posibila(getcoloana(j) + i);

                i++;
                j++;
            }

            i = nr - 1;
            j = patrate[Name].coloana - 1;

            while (i >= 1 && j >= 1)
            {
                if (patrate[getcoloana(j) + i].culoare == culoarea_mea)
                    break;
                else if (patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0)
                {
                    mutare_posibila(getcoloana(j) + i);
                    break;
                }
                else
                    mutare_posibila(getcoloana(j) + i);

                i--;
                j--;
            }

            i = nr + 1;
            j = patrate[Name].coloana - 1;

            while (i <=8 && j >= 1)
            {
                if (patrate[getcoloana(j) + i].culoare == culoarea_mea)
                    break;
                else if (patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0)
                {
                    mutare_posibila(getcoloana(j) + i);
                    break;
                }
                else
                    mutare_posibila(getcoloana(j) + i);

                i++;
                j--;
            }

            i = nr - 1;
            j = patrate[Name].coloana + 1;

            while (i >=1 && j <= 8)
            {
                if (patrate[getcoloana(j) + i].culoare == culoarea_mea)
                    break;
                else if (patrate[getcoloana(j) + i].culoare != culoarea_mea && patrate[getcoloana(j) + i].culoare != 0)
                {
                    mutare_posibila(getcoloana(j) + i);
                    break;
                }
                else
                    mutare_posibila(getcoloana(j) + i);

                i--;
                j++;
            }
        }

        public void mutari_turn(string Name)
        {
            int nr = Convert.ToInt16(Name.Substring(1, 1));

            for (int i = nr - 1; i >= 1; i--)
            {
                if (patrate[Name[0].ToString() + i].culoare == culoarea_mea)
                    break;
                else if (patrate[Name[0].ToString() + i].culoare != culoarea_mea && patrate[Name[0].ToString() + i].culoare != 0)
                {
                    mutare_posibila(Name[0].ToString() + i);
                    break;
                }
                else
                    mutare_posibila(Name[0].ToString() + i);
            }

            for (int i = nr + 1; i <= 8; i++)
            {
                if (patrate[Name[0].ToString() + i].culoare == culoarea_mea)
                    break;
                else if (patrate[Name[0].ToString() + i].culoare != culoarea_mea && patrate[Name[0].ToString() + i].culoare != 0)
                {
                    mutare_posibila(Name[0].ToString() + i);
                    break;
                }
                else
                    mutare_posibila(Name[0].ToString() + i);
            }

            for (int i = patrate[Name].coloana - 1; i >= 1; i--)
            {
                if (patrate[getcoloana(i) + nr].culoare == culoarea_mea)
                    break;
                else if (patrate[getcoloana(i) + nr].culoare != culoarea_mea && patrate[getcoloana(i) + nr].culoare != 0)
                {
                    mutare_posibila(getcoloana(i) + nr);
                    break;
                }
                else
                    mutare_posibila(getcoloana(i) + nr);
            }

            for (int i = patrate[Name].coloana + 1; i <= 8; i++)
            {
                if (patrate[getcoloana(i) + nr].culoare == culoarea_mea)
                    break;
                else if (patrate[getcoloana(i) + nr].culoare != culoarea_mea && patrate[getcoloana(i) + nr].culoare != 0)
                {
                    mutare_posibila(getcoloana(i) + nr);
                    break;
                }
                else
                    mutare_posibila(getcoloana(i) + nr);
            }
        }

        public void mutari_pion(string Name)
        {
            int nr = Convert.ToInt16(Name.Substring(1, 1));

            if (Name[1] == '7')
            {
                if (patrate[Name[0].ToString() + 6].culoare == 0)
                    mutare_posibila(Name[0].ToString() + 6);
                if (patrate[Name[0].ToString() + 5].culoare == 0)
                    mutare_posibila(Name[0].ToString() + 5);
            }
            else if (Name[1] == '1')
            {

            }
            else
            {
                if (patrate[Name[0].ToString() + (nr - 1)].culoare == 0)
                    mutare_posibila(Name[0].ToString() + (nr - 1));
            }

            if (Name[1] != '1')
            {
                string s = getcoloana(patrate[Name].coloana - 1) + (nr - 1);
                if (!getcoloana(patrate[Name].coloana - 1).Equals("") && patrate[s].culoare != 0 && patrate[s].culoare != culoarea_mea)
                {
                    mutare_posibila(s);
                }

                s = getcoloana(patrate[Name].coloana + 1) + (nr - 1);
                if (!getcoloana(patrate[Name].coloana + 1).Equals("") && patrate[s].culoare != 0 && patrate[s].culoare != culoarea_mea)
                {
                    mutare_posibila(s);
                }
            }
        }

        public void history(int culoare, int piesa, string from, string to)
        {
            if (culoare == 1)
            {
                switch (piesa)
                {
                    case 1:
                        ListViewItem it = listView1.Items.Add(from, 0);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                    case 2:
                        it = listView1.Items.Add(from, 1);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                    case 3:
                        it = listView1.Items.Add(from, 2);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                    case 4:
                        it = listView1.Items.Add(from, 3);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                    case 5:
                        it = listView1.Items.Add(from, 4);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                    case 6:
                        it = listView1.Items.Add(from, 5);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                }
            }
            else if (culoare == 2)
            {
                switch (piesa)
                {
                    case 1:
                        ListViewItem it = listView1.Items.Add(from, 6);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                    case 2:
                        it = listView1.Items.Add(from, 7);
                        it.SubItems.Add("--");
                        it.SubItems.Add(to);
                        break;
                    case 3:
                        it = listView1.Items.Add(from, 8);
                        it.SubItems.Add("--");
                        it.SubItems.Add(to);
                        break;
                    case 4:
                        it = listView1.Items.Add(from, 9);
                        it.SubItems.Add("--");
                        it.SubItems.Add(to);
                        break;
                    case 5:
                        it = listView1.Items.Add(from, 10);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                    case 6:
                        it = listView1.Items.Add(from, 11);
                        it.SubItems.Add("-");
                        it.SubItems.Add(to);
                        break;
                }
            }

            listView1.Items[listView1.Items.Count - 1].EnsureVisible();
        }

        public string getcoloana(int nr)
        {
            switch (nr)
            {
                case 1: return "A";
                case 2: return "B";
                case 3: return "C";
                case 4: return "D";
                case 5: return "E";
                case 6: return "F";
                case 7: return "G";
                case 8: return "H";
            }
            return "";
        }

        int culoarea_mea = 1;
        patrat last_picturebox;

        public void mutare_posibila(string locatie)
        {
            if (patrate.ContainsKey(locatie))
            {

                if (patrate[locatie].patrat_negru)
                    patrate[locatie].p.BackColor = Color.DarkGreen;
                else
                    patrate[locatie].p.BackColor = Color.GreenYellow;

                mutari_posibile.Add(locatie);
            }
        }

        public void clear_mutare_posibila(string locatie)
        {
            if (patrate.ContainsKey(locatie))
            {
                if (patrate[locatie].patrat_negru)
                    patrate[locatie].p.BackColor = Color.Gray;
                else
                    patrate[locatie].p.BackColor = Color.White;
            }
        }

        public void set(string locatie, int tip, int culoare)
        {
            //0 = nimic , 1 = alb , 2 = negru

            // 1=pion , 2 = turn, 3 = cal , 4 = nebun, 5 = regina, 6 = rege

            if (patrate.ContainsKey(locatie))
            {
                if (tip == 0)
                {
                    patrate[locatie].p.Image = null;
                }

                if (culoare == 1)
                {
                    switch (tip)
                    {
                        case 1:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\pion.png");
                            break;
                        case 2:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\turn.png");
                            break;
                        case 3:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\cal.png");
                            break;
                        case 4:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\nebun.png");
                            break;
                        case 5:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\regina.png");
                            break;
                        case 6:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\rege.png");
                            break;
                    }
                }
                else if (culoare == 2)
                {
                    switch (tip)
                    {
                        case 1:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\pion_n.png");
                            break;
                        case 2:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\turn_n.png");
                            break;
                        case 3:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\cal_n.png");
                            break;
                        case 4:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\nebun_n.png");
                            break;
                        case 5:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\regina_n.png");
                            break;
                        case 6:
                            patrate[locatie].p.Image = Image.FromFile(Application.StartupPath + "\\images\\rege_n.png");
                            break;
                    }
                }

                patrate[locatie].tip = tip;
                patrate[locatie].culoare = culoare;
            }
        }
    }
}
