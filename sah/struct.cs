﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace sah
{
    public class patrat
    {
        public string nume;
        public PictureBox p;
        //0 = nimic , 1 = alb , 2 = negru
        public int culoare;
        //0 = nimic 1=pion , 2 = turn, 3 = cal , 4 = nebun, 5 = regina, 6 = rege
        public int tip;
        public bool patrat_negru;
        public int coloana;

        public patrat(string nume, PictureBox p, int culoare, int tip, bool patrat_negru, int coloana)
        {
            this.nume = nume;
            this.p = p;
            this.culoare = culoare;
            this.tip = tip;
            this.patrat_negru = patrat_negru;
            this.coloana = coloana;
        }

    }

    public class mutari_computer
    {
        public string from;
        public string to;
        public int scor;

        public mutari_computer(string from,string to, int scor)
        {
            this.from = from;
            this.to = to;
            this.scor = scor;
        }
    }
}
